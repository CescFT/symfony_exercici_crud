<?php

namespace App\Entity;

use App\Repository\DepartamentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=DepartamentRepository::class)
 */
class Departament
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El nom del departament ha d'estar ple")
     */
    private $name_department;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El correu ha d'estar ple")
     * @Assert\Email(message="No té un format de correu")
     */
    private $contact_mail;

    /**
     * @ORM\Column(type="boolean")
     */
    private $weekend_work;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameDepartment(): ?string
    {
        return $this->name_department;
    }

    public function setNameDepartment(string $name_department): self
    {
        $this->name_department = $name_department;

        return $this;
    }

    public function getContactMail(): ?string
    {
        return $this->contact_mail;
    }

    public function setContactMail(string $contact_mail): self
    {
        $this->contact_mail = $contact_mail;

        return $this;
    }

    public function getWeekendWork(): ?bool
    {
        return $this->weekend_work;
    }

    public function setWeekendWork(bool $weekend_work): self
    {
        $this->weekend_work = $weekend_work;

        return $this;
    }
}
