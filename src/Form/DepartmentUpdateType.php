<?php

namespace App\Form;

use App\Entity\Departament;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class DepartmentUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name_department', TextType::class, [
                'label' => 'Nom del departament',
                'empty_data' => '',
                'required' => false,

            ])
            ->add('contact_mail', EmailType::class,[
                'label' => 'Correu electrònic de contacte',
                'empty_data' => '',
                'required' => false,
            ])
            ->add('weekend_work', CheckboxType::class, [
                'label' => 'Treball cap de setmana',
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'attr'=> [
                    'class' =>'btn btn-primary float-right'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Departament::class,
        ]);
    }
}
