<?php

namespace App\Repository;

use App\Entity\Departament;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Departament|null find($id, $lockMode = null, $lockVersion = null)
 * @method Departament|null findOneBy(array $criteria, array $orderBy = null)
 * @method Departament[]    findAll()
 * @method Departament[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartamentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Departament::class);
    }

    public function queryComprovarExistent($nomDepartament, $emailContacte, $treballCapSetmana){
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT d
            FROM App\Entity\Departament d
            WHERE d.name_department = :nomDepartament AND
            d.contact_mail = :emailContacte AND
            d.weekend_work = :capSetmanaTreball'
        )->setParameters(['nomDepartament'=> $nomDepartament, 'emailContacte'=> $emailContacte, 'capSetmanaTreball' =>$treballCapSetmana]);
        return $query->getResult();
    }
    // /**
    //  * @return Departament[] Returns an array of Departament objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Departament
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
