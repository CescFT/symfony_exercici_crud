<?php

namespace App\Controller;

use App\Entity\Departament;
use App\Form\DepartmentType;
use App\Form\DepartmentUpdateType;
use App\Repository\DepartamentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/department", name="department.")
 */
class DepartmentController extends AbstractController
{
    /**
     * @Route("/list_elems", name="llistat")
     */
    public function index(DepartamentRepository $departamentRepository)
    {
        $totsDepartaments = $departamentRepository->findAll();

        return $this->render('department/index.html.twig', [
            'departaments' => $totsDepartaments,
        ]);
    }

    /**
     * @Route("/department_action/{id?}", name="crear")
     */
    public function createDepartment(Request $request, DepartamentRepository $departamentRepository){
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        if($id){
            //entro per fer un update
            $text='Edita';
            $departament = $departamentRepository->find($id);
            if($departament){
                $form = $this ->createForm(DepartmentUpdateType::class, $departament);
                $form -> handleRequest($request);
                if($form->isSubmitted() && $form->isValid()){
                    $nameDepartment = $form['name_department']->getData();
                    $contactMail = $form['contact_mail']->getData();
                    $weekendWork = $form['weekend_work']->getData();
                    if($departamentRepository->queryComprovarExistent($nameDepartment, $contactMail, $weekendWork)){
                        $this->addFlash('errorupdate', 'Aquest departament ja existeix, per tant, no es pot editar amb aquestes dades.');
                    }else{
                        $em->flush();
                        $this->addFlash('update', 'Departament '.$id.' Actualitzat correctament.');
                        return $this->redirect($this->generateUrl('department.llistat'));
                    }
                }
                return $this->render('department/updateDepartment.html.twig', [
                    'formulari' => $form->createView(),
                    'text' => $text
                ]);
            }else{
                return $this->render('department/error.html.twig', [
                    'motiuError'=> 'El departament que estas intentant accedir per editar no existeix.'
                ]);
            }
        }else{
            //entro per crear un nou departament
            $text='Crea';
            $department = new Departament();
            $form = $this ->createForm(DepartmentType::class, $department);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $nameDepartment = $form['name_department']->getData();
                $contactMail = $form['contact_mail']->getData();
                $weekendWork = $form['weekend_work']->getData();
                if($departamentRepository->queryComprovarExistent($nameDepartment, $contactMail, $weekendWork)){
                    $this->addFlash('errorcreate', 'Aquest departament ja existeix.');
                }else{
                    $em->persist($department);
                    $em->flush();
                    $this->addFlash('success', 'S\'ha creat correctament el nou departament');
                    return $this->redirect($this->generateUrl('department.llistat'));
                }
            }
            return $this->render('department/createDepartment.html.twig', [
                'formulari' => $form->createView(),
                'text' => $text
            ]);
        }

    }

    /**
     * @Route("/delete_department", name="eliminar")
     */
    public function deleteDepartment(Request $request, DepartamentRepository $departamentRepository){
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $departament = $departamentRepository->find($id);
        $em->remove($departament);
        $em->flush();
        $tots_departaments = $departamentRepository->findAll();
        $llistat_departaments = array();

        foreach ($tots_departaments as $departament){
            if($departament->getWeekendWork()){
                $s='id|'.$departament->getId().'||nomdepartament|'.$departament->getNameDepartment().'||contactmail|'.$departament->getContactMail().'||weekendwork|si||act||del';
            }else{
                $s='id|'.$departament->getId().'||nomdepartament|'.$departament->getNameDepartment().'||contactmail|'.$departament->getContactMail().'||weekendwork|no||act||del';
            }
            $llistat_departaments[] = $s;
        }

        return new JsonResponse(
            array('existents'=> $llistat_departaments)
        );

    }
}
